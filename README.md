The Viterbi Algorithm is a dynamic programming algorithm for finding the most likely sequence of hidden states – called the Viterbi path – that results in a sequence of observed events, especially in the context of Markov information sources and hidden Markov models.

When an input or probabilities are prone to underflow, the log-sum exp trick solves the issue by taking natural log of probabilities.

If you wish to see output on console, you can comment out @Before stream() at the top of the test file (lines 20 - 25).

You may test the algorithm using your own probability matrices A, B, and pi, and omega (array of output sequences) in ViterbiTest.java.

Download and add path to guava-20.0.jar (https://github.com/google/guava/wiki/Release20) for Stopwatch feature in the test file.